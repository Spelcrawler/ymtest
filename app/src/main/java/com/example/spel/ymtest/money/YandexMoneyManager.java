package com.example.spel.ymtest.money;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yandex.money.api.authorization.AuthorizationData;
import com.yandex.money.api.authorization.AuthorizationParameters;
import com.yandex.money.api.methods.Token;
import com.yandex.money.api.model.Scope;
import com.yandex.money.api.net.AuthorizationCodeResponse;
import com.yandex.money.api.net.ParametersBuffer;
import com.yandex.money.api.net.clients.ApiClient;
import com.yandex.money.api.net.clients.DefaultApiClient;
import com.yandex.money.api.net.providers.HostsProvider;
import com.yandex.money.api.util.Log;

import java.util.HashMap;
import java.util.Map;

import static com.example.spel.ymtest.money.LocalTokenProvider.TAG;

public class YandexMoneyManager {

    private static final String RESPONSE_TYPE_CODE = "code";

    private String mRedirectUrl;
    private ApiClient mClient;
    private TokenProvider mTokenProvider;
    private String mShopId;
    private String mScid;
    private @Nullable PaymentCallback mPaymentCallback;
    private boolean mDebug;

    public static Builder newBuilder() {
        return new YandexMoneyManager().new Builder();
    }

    private YandexMoneyManager() {}

    public void setPaymentCallback(@Nullable PaymentCallback paymentCallback) {
        mPaymentCallback = paymentCallback;
    }

    public void startPayment(WebView webView) {
        if (mTokenProvider.getToken() == null) {
            startAuth(webView);
        } else {
            executePayment(webView, getParams());
        }
    }

    private HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("pattern_id", "phone-topup");
        params.put("phone-number", "79219990099");
        params.put("amount", "1000.00");
        params.put("test_payment", "true");
        params.put("test_result", "success");

        return params;
    }

    private void executePayment(WebView webView, Map<String, String> postData) {
        setupPaymentWebView(webView);
        webView.postUrl("https://demomoney.yandex.ru/api/request-payment", generatePaymentParams(postData));
    }

    private byte[] generatePaymentParams(Map<String, String> postData) {
        return new ParametersBuffer().setParameters(postData).prepareBytes();
    }

    private void startAuth(WebView webView) {
        AuthorizationParameters parameters = createAuthParameters(mRedirectUrl);
        AuthorizationData data = mClient.createAuthorizationData(parameters);

        setupAuthWebView(webView);
        webView.postUrl(data.getUrl(), data.getParameters());
    }

    private void setClientId(String clientId) {
        mClient = createApiClient(clientId);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupAuthWebView(WebView webView) {
        webView.setWebViewClient(new AuthClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupPaymentWebView(WebView webView) {
        webView.setWebViewClient(new PaymentClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private ApiClient createApiClient(String clientId) {
        return new DefaultApiClient.Builder().setClientId(clientId).setDebugMode(true).setHostsProvider(new HostProvider(mDebug)).create();
    }

    private AuthorizationParameters createAuthParameters(String redirectUrl) {
        return new AuthorizationParameters.Builder()
                .addScope(Scope.ACCOUNT_INFO)
                .addScope(Scope.PAYMENT_P2P)
                .addScope(Scope.createPaymentShopLimitedScope())
                .setResponseType(RESPONSE_TYPE_CODE)
                .setRedirectUri(redirectUrl)
                .build();
    }

    private class AuthClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            android.util.Log.d(TAG, "redirectUrl: " + url);
            if (url.startsWith(mRedirectUrl)) {
                new TokenTask(view, mClient).execute(url);
                return true;
            }

            return false;
        }
    }

    private class PaymentClient extends WebViewClient {

    }

    public class Builder {

        private Builder() {}

        public Builder setShopId(String shopId) {
            YandexMoneyManager.this.mShopId = shopId;
            return this;
        }

        public Builder setScid(String scid) {
            YandexMoneyManager.this.mScid = scid;
            return this;
        }

        public Builder setPaymentCallback(PaymentCallback callback) {
            YandexMoneyManager.this.mPaymentCallback = callback;
            return this;
        }

        public Builder setRedirectUrl(String redirectUrl) {
            YandexMoneyManager.this.mRedirectUrl = redirectUrl;
            return this;
        }

        public Builder setClientId(String clientId) {
            YandexMoneyManager.this.setClientId(clientId);
            return this;
        }

        public Builder setTokenProvider(TokenProvider tokenProvider) {
            YandexMoneyManager.this.mTokenProvider = tokenProvider;
            return this;
        }

        public Builder setDebugMode(boolean debug) {
            YandexMoneyManager.this.mDebug = debug;
            return this;
        }

        public YandexMoneyManager build() {
            return YandexMoneyManager.this;
        }

    }

    private class TokenTask extends AsyncTask<String, Void, String> {

        private final WebView mWebView;
        private final ApiClient mClient;

        private TokenTask(WebView webView, ApiClient client) {
            mWebView = webView;
            mClient = client;

        }

        @Override
        protected String doInBackground(String... strings) {
            String url = strings[0];
            try {
                return requestAccessToken(mClient, url);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String token) {
            if (token == null) {
                if (mPaymentCallback != null) mPaymentCallback.onPaymentError();
            } else {
                mTokenProvider.storeToken(token);
                startPayment(mWebView);
            }
        }

        private String requestAccessToken(ApiClient client, String redirectUrl) throws Exception {
            Token token = getToken(client, redirectUrl);
            if (token != null && token.error == null) {
                return token.accessToken;
            }

            return null;
        }

        private @Nullable Token getToken(ApiClient client, String redirectUri) throws Exception {
            AuthorizationCodeResponse response = AuthorizationCodeResponse.parse(redirectUri);
            if (response.error == null) {
                Token.Request request = new Token.Request(response.code, client.getClientId(), redirectUri);
                return mClient.execute(request);
            }

            return null;
        }
    }

    private class HostProvider implements HostsProvider {

        private static final String MONEY_URL = "https://money.yandex.ru";
        private static final String DEMO_MONEY_URL = "https://demomoney.yandex.ru";

        private boolean mDebug;

        public HostProvider(boolean debug) {
            mDebug = debug;
        }

        @Override
        public String getMoney() {
            return mDebug ? DEMO_MONEY_URL : MONEY_URL;
        }

        @Override
        public String getMoneyApi() {
            return null;
        }

        @Override
        public String getPaymentApi() {
            return null;
        }

        @Override
        public String getMobileMoney() {
            return null;
        }

        @Override
        public String getWebUrl() {
            return null;
        }

    }


    public interface PaymentCallback {
        void onPaymentSuccess();
        void onPaymentError();
    }

}
