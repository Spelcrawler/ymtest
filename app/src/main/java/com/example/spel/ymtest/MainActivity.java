package com.example.spel.ymtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.spel.ymtest.money.LocalTokenProvider;
import com.example.spel.ymtest.money.YandexMoneyManager;

public class MainActivity extends AppCompatActivity implements YandexMoneyManager.PaymentCallback {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String CLIENT_ID = "CEDBB8F0D4143DEA2C121A8D4A9DA4E379C490D6F5D7206F3A3CA857DF56CDD4";
    private static final String DEBUG_CLIENT_ID = "6BB13DDBB8051F6B0CA8478A263B9D20210FC7DC445D43D37D44A910F503DEA2";
    private static final String REDIRECT_URL = "https://debugmoney.yandex.ru";
    private static final String SHOP_ID = "124567";
    private static final String SCID = "549844";
    private static final String TOKEN = "FACA36CAFFDEF2119BEB364DCEAF1867DEDAB5AD3CE7F18D8D430750E3FD572FE800C5F813C90BABDE689362AEE40EDD65FBD58CEFC80B3C3B20D226F01B71B722D94CE5DE39D805A78D7DE1765B5FABC06E614B4CCD824F39B7ED8A142B6FAF706D31210CF681440F817AA025D58FB04B329E9CB6F1E176C5738B16BD59A607";

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = (WebView) findViewById(R.id.web_view);

        YandexMoneyManager manager = YandexMoneyManager.newBuilder()
                .setDebugMode(true)
                .setClientId(DEBUG_CLIENT_ID)
                .setPaymentCallback(this)
                .setTokenProvider(new LocalTokenProvider())
                .setRedirectUrl(REDIRECT_URL)
                .setShopId(SHOP_ID)
                .setScid(SCID)
                .build();

        manager.setPaymentCallback(this);
        manager.startPayment(mWebView);
    }

    @Override
    public void onPaymentSuccess() {
        mWebView.setVisibility(View.GONE);
        Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPaymentError() {
        mWebView.setVisibility(View.GONE);
        Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show();
    }
}
