package com.example.spel.ymtest.money;

public interface TokenProvider {

    void storeToken(String token);
    String getToken();

}
