package com.example.spel.ymtest.money;

import android.util.Log;

/**
 * Created by Spel on 21.03.2017.
 */

public class LocalTokenProvider implements TokenProvider {

    public static final String TAG = LocalTokenProvider.class.getSimpleName();

    private String mToken;

    @Override
    public void storeToken(String token) {
        Log.d(TAG, "Token stored: " + token);
        mToken = token;
    }

    @Override
    public String getToken() {
        return mToken;
    }

}
